//
//  ToiletRepository.swift
//  Paris Pee
//
//  Created by Ramzy Kermad on 23/10/2021.
//

import Foundation
import Alamofire
import RxSwift
import RxCocoa

class ToiletRepository {
    
    private let baseUrl = "https://data.ratp.fr/api/records/1.0/search/?dataset=sanisettesparis2011"
    
    
    func getParisToiletList() -> Observable<[ParisToiletModel]> {
       
        return Observable<[ParisToiletModel]>.create({observer in
            AF.request(self.baseUrl, method: .get)
                .validate(statusCode: 200..<300)
                .responseJSON { response in
                    switch response.result {
                        case .success( _):
                        guard let responseData =  response.value as? [String: Any],
                              let records = responseData["records"] as? [[String: Any]] else {
                                return observer.onCompleted()
                            }
                        var toiletModelList = [ParisToiletModel]()
                            
                        for record in records{
                            guard let recordField = record["fields"] as? [String: Any],
                                let street = recordField["adresse"] as? String,
                                let location = recordField["geo_point_2d"] as? [Double],
                                let pmrAccess = recordField["acces_pmr"] as? String,
                                let openingHours = recordField["horaire"] as? String
                            else{
                                return observer.onCompleted()
                            }
                            let district = recordField["arrondissement"] as? Int
                            let toiletAddress = ToiletAddress(district: district, street: street)
                            
                            let toiletModel = ParisToiletModel(address: toiletAddress,
                                                               accessPmr: pmrAccess,
                                                               openingHours: openingHours,
                                                               location: location)
                            toiletModelList.append(toiletModel)
                        }
                        
                        observer.onNext(toiletModelList)
                        case .failure(_):
                            print("ERROR ALAMOFIRE | getParisToiletList() | ToiletRepository")
                    }
                }

            return Disposables.create();
        })
    }
}
