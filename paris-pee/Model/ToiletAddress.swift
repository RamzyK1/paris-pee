//
//  ToiletAddress.swift
//  Paris Pee
//
//  Created by Ramzy Kermad on 23/10/2021.
//

import Foundation

struct ToiletAddress: Codable {
    
    let district: Int?
    let street: String
    
    enum CodingKeys: String, CodingKey {
        case district = "arrondissement"
        case street = "adresse"
    }
    
    func toString() -> String {
        if(district == nil){
            return street
        }else{
            return street + " " + district!.description
        }
    }
    
}
