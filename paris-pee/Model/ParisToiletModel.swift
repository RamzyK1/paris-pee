//
//  ParisToiletModel.swift
//  Paris Pee
//
//  Created by Ramzy Kermad on 23/10/2021.
//

import Foundation
import CoreLocation

class ParisToiletModel {
    
    let address: ToiletAddress
    let isPmrAccessible: Bool
    let openingHours: String
    let location: [Double]
    
    init(address: ToiletAddress, accessPmr: String, openingHours: String, location: [Double]){
        self.address = address
        self.isPmrAccessible = (accessPmr == "Oui")
        self.openingHours = openingHours
        self.location = location
        _ = self.location.sorted { loc1, loc2 in
            loc1 > loc2
        }
    }

    
    /**
        Function returning the distance in meters between toilet and user location
     */
    func getDistanceFrom(_ userPosition: CLLocationCoordinate2D) -> String {
        let toiletLocation = CLLocation(latitude: location[0], longitude: location[1])
        let userLocation = CLLocation(latitude: userPosition.latitude, longitude: userPosition.longitude)
        
        let distanceToToilet = round(toiletLocation.distance(from: userLocation))
        let unit = distanceToToilet < 1000 ? "m" : "Km"
        let formatedDistance = distanceToToilet < 1000 ? distanceToToilet : round(distanceToToilet / 1000)
        
        return " \(formatedDistance) \(unit)"
    }
    

}
