//
//  ToiletLocation.swift
//  Paris Pee
//
//  Created by Ramzy Kermad on 23/10/2021.
//

import Foundation

struct ToiletLocation {
    let longitude: Double
    let latitude: Double
    
    init(long: Double, lat: Double){
        self.longitude = long
        self.latitude = lat
    }
}
