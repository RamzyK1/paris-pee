//
//  ToiletViewModel.swift
//  Paris Pee
//
//  Created by Ramzy Kermad on 23/10/2021.
//

import Foundation
import RxCocoa
import RxSwift

class ToiletViewModel {
    let toiletRepository = ToiletRepository()
    
    let toiletList = BehaviorSubject<[ParisToiletModel]>(value: [])
    
    func getParisToilet() {
        _ = toiletRepository.getParisToiletList().subscribe { parisToilets in
            self.toiletList.onNext(parisToilets)
        } onError: { error in
            print("\(error) | getParisToilet() | ToiletViewModel")
        }
    }
}
