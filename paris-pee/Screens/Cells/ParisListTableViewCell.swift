//
//  ParisListTableViewCell.swift
//  Paris Pee
//
//  Created by Ramzy Kermad on 25/10/2021.
//

import Foundation
import UIKit

class ParisListTableViewCell: UITableViewCell{
    static let identifier = "PARIS_TOILET_CUSTOM_CELL"
    var cellData: ParisToiletModel? = nil {
        didSet {
            guard let address = cellData?.address,
                  let openingHours = cellData?.openingHours,
                  let pmrAccess = cellData?.isPmrAccessible
            else{
                return
            }
            addressLabel.text = address.toString()
            openingHourLabel.text = openingHours
            if pmrAccess {
                pmrAccessImage.image = UIImage(named: "access_pmr")
            }else{
                pmrAccessImage.image = UIImage(named: "no_access_pmr")
            }
        }
    }
    
    let addressLabel: UILabel = {
        let label = UILabel()
        label.textColor = .label
        return label
    }()
    
    let openingHourLabel: UILabel = {
        let label = UILabel()
        label.textColor = .label
        return label
    }()
    
    let distanceLabel: UILabel = {
        let label = UILabel()
        label.textColor = .label
        return label
    }()
    
    let pmrAccessImage: UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(addressLabel)
        contentView.addSubview(openingHourLabel)
        contentView.addSubview(distanceLabel)
        contentView.addSubview(pmrAccessImage)
        contentView.clipsToBounds = true
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let screenWidth = UIScreen.main.bounds.width
        addressLabel.frame = CGRect(x: 10, y: 10, width: (screenWidth / 3) * 2, height: 30)
        pmrAccessImage.frame = CGRect(x: 10, y: 40, width: 30, height: 30)
        openingHourLabel.frame = CGRect(x: 40, y: 40, width: screenWidth / 3, height: 30)
        distanceLabel.frame = CGRect(x: screenWidth - (screenWidth / 4), y: 10, width: screenWidth / 3, height: 30)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
}
