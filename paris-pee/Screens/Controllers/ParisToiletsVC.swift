//
//  ParisToiletsVC.swift
//  Paris Pee
//
//  Created by Ramzy Kermad on 23/10/2021.
//

import UIKit
import CoreLocation

class ParisToiletsVC: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var toiletTableView: UITableView!
    
    let toiletVM = ToiletViewModel()
    
    // MARK: Local Data
    private var parisToilets: [ParisToiletModel] = []
    private var parisToiletsFiltered: [ParisToiletModel] = []
    private var isToiletFiltered = false
    
    // MARK: - Location
    private var userLocation: CLLocationCoordinate2D?
    private var locationManager: CLLocationManager = CLLocationManager()
    private var requestLocationAuthorizationCallback: ((CLAuthorizationStatus) -> Void)?
    
    var filterButton: UIBarButtonItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        setUpListeners()
        toiletVM.getParisToilet()
    }
    
    private func setUpView(){
        setUpUI()
        requestLocationAuthorization()
        toiletTableViewSetup()
    }
    
    private func setUpUI(){
        // Do all the customisation of the UI here
        self.title = "Paris-Pee"
        setUpNavigationBarButton()
    }
    
    private func setUpNavigationBarButton(){
        filterButton = UIBarButtonItem(image: UIImage(systemName: "line.3.horizontal.decrease.circle"), style: .done, target: self, action: #selector(self.filterListByPmrAccessible))
        self.navigationItem.rightBarButtonItem = filterButton
        
    }
    
    @objc private func filterListByPmrAccessible(){
        self.isToiletFiltered = !isToiletFiltered
        self.parisToiletsFiltered.removeAll()
        
        if isToiletFiltered {
            filterButton?.image = UIImage(systemName: "line.3.horizontal.decrease.circle.fill")
            self.parisToiletsFiltered = self.parisToilets.filter({ toilet in
                return toilet.isPmrAccessible
            })
        }else{
            filterButton?.image = UIImage(systemName: "line.3.horizontal.decrease.circle")
            self.parisToiletsFiltered = parisToilets
        }
        toiletTableView.reloadData()
    }
    
    private func toiletTableViewSetup(){
        toiletTableView.register(ParisListTableViewCell.self,
                                 forCellReuseIdentifier: ParisListTableViewCell.identifier)
        toiletTableView.dataSource = self
        toiletTableView.delegate = self
    }
    
    public func requestLocationAuthorization() {
        self.locationManager.delegate = self
        let currentStatus = CLLocationManager.authorizationStatus()

        guard currentStatus == .notDetermined else {
            return
        }

        if #available(iOS 13.4, *) {
            self.requestLocationAuthorizationCallback = { status in
                if status == .authorizedWhenInUse {
                    self.locationManager.requestAlwaysAuthorization()
                }
            }
            self.locationManager.requestWhenInUseAuthorization()
        } else {
            self.locationManager.requestAlwaysAuthorization()
        }
    }
    
    private func setUpListeners(){
        _ = self.toiletVM.toiletList.subscribe { response in
            guard let toilets = response.element else{
                return
            }
            self.parisToilets = toilets
            self.parisToiletsFiltered = self.parisToilets
            self.toiletTableView.reloadData()
        }
    }
    
    public func locationManager(_ manager: CLLocationManager,
                                didChangeAuthorization status: CLAuthorizationStatus) {
        self.requestLocationAuthorizationCallback?(status)
    }

}

extension ParisToiletsVC: UITableViewDelegate {
    
}

extension ParisToiletsVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return parisToiletsFiltered.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ParisListTableViewCell.identifier) as! ParisListTableViewCell
        
        let currentToiletModel = self.parisToiletsFiltered[indexPath.row]
        let pmrAccess = currentToiletModel.isPmrAccessible
        cell.addressLabel.text = currentToiletModel.address.toString()
        cell.openingHourLabel.text = currentToiletModel.openingHours
        
        if pmrAccess {
            cell.pmrAccessImage.image = UIImage(named: "access_pmr")
        }else{
            cell.pmrAccessImage.image = UIImage(named: "no_access_pmr")
        }
        
        guard let userLocation: CLLocationCoordinate2D = locationManager.location?.coordinate else {
            return UITableViewCell()
        }
        cell.distanceLabel.text = currentToiletModel.getDistanceFrom(userLocation)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let toiletDetailVC = ParisToiletDetailVC()
        let selectedToilet = self.parisToiletsFiltered[indexPath.row]
        toiletDetailVC.toiletModel = selectedToilet
        self.navigationController?.pushViewController(toiletDetailVC, animated: true)
        
        tableView.cellForRow(at: indexPath)?.isSelected = false
    }
}
