//
//  ParisToiletDetailVC.swift
//  Paris Pee
//
//  Created by Ramzy Kermad on 24/10/2021.
//

import UIKit
import MapKit

class ParisToiletDetailVC: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var mkMapView: MKMapView!
    @IBOutlet weak var toiletAddressLabel: UILabel!
    @IBOutlet weak var pmrIcon: UIImageView!
    @IBOutlet weak var openingTimesLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    var toiletModel: ParisToiletModel?
    private var userLocation: CLLocationCoordinate2D?
    
    private var locationManager: CLLocationManager = CLLocationManager()
    private var requestLocationAuthorizationCallback: ((CLAuthorizationStatus) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpViews()
        requestLocationAuthorization()
    }

    private func setUpViews(){
        toiletAddressLabel.text = toiletModel?.address.toString()
        openingTimesLabel.text = "Horaires d'ouverture: \(toiletModel?.openingHours ?? " - ")"
        setUpDistanceLabel()
        setUpPmrIcon()
        showToiletOnMap()
    }
    
    private func setUpDistanceLabel(){
        switch locationManager.authorizationStatus {
            case .denied, .notDetermined:
                let alert = UIAlertController(title: "Attention", message: "En refusant, la permission de localisation vous pourriez ne pas avoir certaines informations", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "D'accord", style: .default) { alert in
                    
                }
                alert.addAction(okAction)
                alert.show(self, sender: self)
            default:
                guard let userLocation: CLLocationCoordinate2D = locationManager.location?.coordinate else {
                    return
                }
                let distanceToToilet = toiletModel?.getDistanceFrom(userLocation)
            distanceLabel.text = "Distance: \(distanceToToilet ?? "")"
        }
    }
    
    private func setUpPmrIcon(){
        guard let pmr_access = toiletModel?.isPmrAccessible else {
            pmrIcon.image = UIImage(named: "no_access_pmr")
            return
        }
        if pmr_access {
            pmrIcon.image = UIImage(named: "access_pmr")
        }else{
            pmrIcon.image = UIImage(named: "no_access_pmr")
        }
    }
    
    private func showToiletOnMap(){
        let toiletPosition = CLLocationCoordinate2D(latitude: (toiletModel?.location[0])!,
                                                    longitude: (toiletModel?.location[1])!)
        let toiletPinAnnotation = MKPointAnnotation()
        toiletPinAnnotation.title = "Toilette publique de la Ville de Paris"
        toiletPinAnnotation.subtitle = toiletModel?.address.street
        
        toiletPinAnnotation.coordinate = toiletPosition
        mkMapView.addAnnotation(toiletPinAnnotation)
        
        let region = MKCoordinateRegion(
              center: toiletPosition,
              latitudinalMeters: 500,
              longitudinalMeters: 500)
        mkMapView.setRegion(region, animated: true)
    }

    public func requestLocationAuthorization() {
        self.locationManager.delegate = self
        let currentStatus = CLLocationManager.authorizationStatus()

        guard currentStatus == .notDetermined else {
            return
        }

        if #available(iOS 13.4, *) {
            self.requestLocationAuthorizationCallback = { status in
                if status == .authorizedWhenInUse {
                    self.locationManager.requestAlwaysAuthorization()
                }
            }
            self.locationManager.requestWhenInUseAuthorization()
        } else {
            self.locationManager.requestAlwaysAuthorization()
        }
    }
    
    public func locationManager(_ manager: CLLocationManager,
                                didChangeAuthorization status: CLAuthorizationStatus) {
        self.requestLocationAuthorizationCallback?(status)
    }
}
